# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import password_change, password_change_done
from django.views.generic import TemplateView
from django.views import defaults as default_views

from mercatitulo import views as views_home
from mercatitulo.derechos import views as views_derechos
from mercatitulo.proyectos import views as views_proyectos
from mercatitulo.users import views as views_users
from mercatitulo.contacto import views as views_contacto

urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    # User management
    url(r'^password-change/$', password_change, {
        'template_name': 'registration/password_change_form.html'},
        name='password_change'),
    url(r'^password_change_done/$', password_change_done, {
        'template_name': 'registration/password_change_done.html'}, 
        name='password_change_done'),
    url(r'^users/', include('mercatitulo.users.urls', namespace='users')),
    url(r'^accounts/signup/', views_users.SignupMercatitulo.as_view(), name='signup'),
    url(r'^accounts/login/', views_users.LoginMercatitulo.as_view(), name='login'),
    url(r'^mi-cuenta/$', views_users.MiCuenta, name='mi_cuenta'),    
    url(r'^accounts/', include('allauth.urls')),
    url(r'^logout/$', views_home.Logout, name='logout'),

    # Tinymce
    url(r'^tinymce/', include('tinymce.urls')),

    # Your stuff: custom urls includes go here
    url(r'^$', views_home.Landing, name="landing"),
    url(r'^home2$', views_home.Home, name="home"),
    url(r'^quienes-somos/',
        TemplateView.as_view(template_name="pages/quienes_somos.html"), name='quienes'),
    url(r'^mercado-derechos/$', views_derechos.Mercado, name='mercado'),
    url(r'^mercado-derechos/(?P<slug>[\w-]+)/$', views_derechos.Detalle, name='derecho_detalle'),
    url(r'^nuevos-proyectos/$', views_proyectos.Proyectos, name='proyectos'),
    url(r'^proyecto/confirmacion/$', views_proyectos.Confirmacion, name='proyecto_confirmacion'),

    # Vender
    url(r'^vender/$', views_derechos.Vender, name='vender'),
    url(r'^vender/proyecto_nuevo/$',
        views_proyectos.NuevoProyecto, name='nuevo_proyecto'),
    url(r'^vender/confirmacion/$', views_derechos.Confirmacion, name='confirmacion'),

    # Comprar
    url(r'^comprar/$', views_derechos.Comprar, name='comprar'),
    url(r'^ofertar/(?P<slug>[\w-]+)/$', views_derechos.Ofertar, name='ofertar'),
    url(r'^save-oferta/$', views_derechos.SaveOferta, name='save-oferta'),
    url(r'^save-oferta/(?P<llave>[-:\w]+)/$', views_derechos.ConfirmOferta, name='confirm-oferta'),
    url(r'^detalle-compra/$', views_derechos.detalleCompra, name='detalle-compra'),
    # Ajax
    url(r'^ajax/load_proyectos/$',
        views_proyectos.LoadProyectos, name='load_proyectos'),

    #Editar eliminar proyectos
    url(r'^proyecto/editar/(?P<slug>[\w-]+)/$', views_proyectos.EditarProyectos, name='editar_proyecto'),
    url(r'^proyecto/eliminar/(?P<slug>[\w-]+)/$', views_proyectos.EliminarProyectos, name='eliminar_proyecto'),

    #Editar eliminar derechos
    url(r'^derechos/editar/(?P<slug>[\w-]+)/$', views_derechos.EditarDerechos, name='editar_derecho'),
    url(r'^derechos/eliminar/(?P<slug>[\w-]+)/$', views_derechos.EliminarDerechos, name='eliminar_derecho'),

    #Contacto
    url(r'^contacto/$', views_contacto.Contacto, name='contacto'),

    #Compania
    url(r'^derechos-que-son/$', TemplateView.as_view(template_name='pages/derechos_que_son.html'), name='derechos_que_son'),
    url(r'^como-funciona/$', TemplateView.as_view(template_name='pages/como_funciona.html'), name='como_funciona'),
    url(r'^riesgos-beneficios/$', TemplateView.as_view(template_name='pages/riesgos_beneficios.html'), name='riesgos_beneficios')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
