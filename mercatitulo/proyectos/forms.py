# -*- coding: utf-8 -*-
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, HTML
from crispy_forms.bootstrap import FormActions

from mercatitulo.proyectos.models import Proyecto

class NuevoProyectoForm(forms.ModelForm):
    class Meta:
        model = Proyecto
        widgets = {
            'precio': forms.TextInput(attrs={'class': 'field-precio'}),
            'rendimiento': forms.TextInput(attrs={'class': 'field-rendimiento'}),
        }
        fields = [
            'nombre',
            'tipo',
            'fiduciaria',
            'marca',
            'precio',
            'rendimiento',
            'foto',
            'descripcion',
        ]

    def __init__(self, *args, **kwargs):
        super(NuevoProyectoForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout.append(
            FormActions(
                HTML("""<a role="button" class="btn btn-default"
                        href="{% url "home" %}">Cancelar</a>"""),
                Submit('save', 'Crear un nuevo proyecto'),
            )
        )

class EditarProyectoForm(NuevoProyectoForm):
    def __init__(self, *args, **kwargs):
        super(EditarProyectoForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout.append(
            FormActions(
                HTML("""<a role="button" class="btn btn-default"
                        href="{% url "mi_cuenta" %}">Cancelar</a>"""),
                Submit('save', 'Actualizar'),
            )
        )

class BuscarProyectoForm(forms.Form):
    nombre = forms.CharField()
    helper = FormHelper()
    helper.label_class = 'hide'
    helper.form_method = 'GET'
    helper.add_input(Submit('enviar', 'Buscar', css_class='btn-primary'))

    def __init__(self, *args, **kwargs):
        super(BuscarProyectoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs[
            'placeholder'] = "Nombre del Proyecto"
