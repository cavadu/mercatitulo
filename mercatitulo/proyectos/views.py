# -*- coding: utf-8 -*-

from django.contrib import messages
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404

from mercatitulo.proyectos.forms import NuevoProyectoForm, BuscarProyectoForm, EditarProyectoForm
from mercatitulo.proyectos.models import Proyecto, TipoProyecto, ESTADOS as estado_proyecto
from mercatitulo.derechos.utils import Cast, Send_mail
from mercatitulo.users.models import User



def LoadProyectos(request):
	""" Vista ajax que carga el select de proyectos segun criterios """
	if 'tipo' in request.GET.keys() and 'marca' in request.GET.keys():
		try:
			exclude = request.GET['exclude']
			tipo_id = request.GET['tipo']
			marca_id = request.GET['marca']
			proyectos = Proyecto.objects.filter(
				tipo=tipo_id, marca=marca_id, estado=1).values()
		except ObjectDoesNotExist:
			proyectos = False

		data = {
			'proyectos': proyectos,
			'exclude': exclude,
		}
		return render(request, 'ajax/proyectos_sel.html', {'data': data})
	else:
		return HttpResponse("No hay nada que ver")

def EliminarProyectos(request, slug):
	proyecto = get_object_or_404(Proyecto, slug=slug)
	data = {
        'proyecto': proyecto,
    }
	if request.method == "POST":
		proyecto.delete()
		messages.success(request, 'El proyecto ha sido eliminado')
		redirect_to = '/mi-cuenta/?opc=proyectos'
		return redirect(redirect_to)
	else:
		return render(request, 'pages/eliminar_proyectos.html', {'data': data})

def EditarProyectos(request, slug):
	proyecto = get_object_or_404(Proyecto, slug=slug)
	data = {
        'proyecto': proyecto,
    }
	if request.method == "POST":
		post = request.POST.copy()
		if not post['precio'] == '$ 0':
			post['precio'] = Cast(post['precio'])
		if not post['rendimiento'] == '$ 0':
			post['rendimiento'] = Cast(post['rendimiento'])
		form = EditarProyectoForm(post, request.FILES, instance=proyecto)
		if form.is_valid():
			form.save()
			messages.success(request, 'El proyecto ha sido modificado')
			redirect_to = '/mi-cuenta/?opc=proyectos'
			return redirect(redirect_to)
	else:
		form = EditarProyectoForm(instance=proyecto)
	return render(request, 'forms/editar_proyecto.html', {'form': form, 'data': data})

def NuevoProyecto(request):
	if request.method == "POST":
		post = request.POST.copy()
		if not post['precio'] == '$ 0':
			post['precio'] = Cast(post['precio'])
		if not post['rendimiento'] == '$ 0':
			post['rendimiento'] = Cast(post['rendimiento'])
		form = NuevoProyectoForm(post, request.FILES)
		if form.is_valid():
			nuevo_proyecto = form.save()
			request.session['proyecto_id'] = nuevo_proyecto.id
			if request.user.is_authenticated():
				proyecto = Proyecto.objects.get(pk=nuevo_proyecto.id)
				proyecto.autor_id = request.user.id
				proyecto.save()
				Send_mail(
					{ 
						'proyecto_id': nuevo_proyecto.id,
						'proyecto_name': nuevo_proyecto.nombre,
						'username': request.user.username,
					},
					[request.user.email],
					'partials/email/asignacion_proyecto.txt',
					'partials/email/asignacion_proyecto.html',
					'Nuevo Proyecto',
				)
				redirect_to = '/proyecto/confirmacion/'
			else:
				redirect_to = '/accounts/signup/'
			usuarios_correo = User.objects.filter(is_staff=True)
			destino_correo = []
			for usuario in usuarios_correo:
				destino_correo.append(str(usuario.email))
			if request.user.username:
				usuario = request.user.username
			else:
				usuario = 'Pendiente asignar usuario'
			Send_mail(
				{ 
					'proyecto_id': nuevo_proyecto.id,
					'proyecto_name': nuevo_proyecto.nombre,
					'username': usuario,
				},
				destino_correo,
				'partials/email/asignacion_proyecto_admin.txt',
				'partials/email/asignacion_proyecto_admin.html',
				'Nuevo Proyecto',
			)
			return redirect(redirect_to)
	else:
		form = NuevoProyectoForm()
	return render(request, 'forms/nuevo_proyecto.html', {'form': form})

def Confirmacion(request):
	if request.session.get('proyecto_id', None):
		proyecto = Proyecto.objects.get(pk=request.session['proyecto_id'])
		del request.session['proyecto_id']
		data = {
			'proyecto': proyecto,
			'estado': dict(estado_proyecto).get(proyecto.estado),
		}
		return render(request, 'pages/proyecto_confirmacion.html', {'data': data})
	else:
		redirect_to = '/'
		return redirect(redirect_to)


def Proyectos(request):
	form = BuscarProyectoForm()
	if 'tipos_proyecto' in request.GET.keys():
		tipos_proyecto_id = request.GET['tipos_proyecto']
		proyectos = Proyecto.objects.filter(estado=1, autor__isnull=False, tipo=tipos_proyecto_id)
	elif 'nombre' in request.GET.keys():
		nombre = request.GET['nombre']
		proyectos = Proyecto.objects.filter(estado=1, autor__isnull=False, nombre=nombre)
	else:
		proyectos = Proyecto.objects.filter(estado=1, autor__isnull=False)
	tipos_de_proyecto = TipoProyecto.objects.all().order_by('nombre')
	data = {
		'proyectos': proyectos,
		'tipos_proyecto': tipos_de_proyecto,
		'buscar_proyecto_form': form,
	}
	return render(request, 'pages/proyectos.html', {'data': data})
