# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from autoslug import AutoSlugField
from tinymce.models import HTMLField

from mercatitulo.fiduciarias.models import Fiduciaria

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

ESTADOS = (
    ('0', 'Por aprobar'),
    ('1', 'Aprobado'),
)

ESTADOS_COMERCIALES = (
    ('0', 'Por construir'),
    ('1', 'En construcción'),
    ('2', 'En operación'),
)

@python_2_unicode_compatible
class TipoProyecto(models.Model):
    """ Clase para clasificar a los proyectos """
    nombre = models.CharField(default=None, max_length=50)
    descripcion = HTMLField()

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class MarcaProyecto(models.Model):
    """ Clase para clasificar a los proyectos por marca """
    nombre = models.CharField(default=None, max_length=50)
    descripcion = HTMLField()

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class Proyecto(models.Model):
    """Clase para hacer administrables los proyectos"""
    nombre = models.CharField(
        max_length=140, verbose_name='Nombre del proyecto')
    tipo = models.ManyToManyField(
        TipoProyecto, default=None, verbose_name='Tipo de inmueble')
    marca = models.ForeignKey(
        MarcaProyecto, default=None, verbose_name='Marca comercial')
    precio = models.FloatField(default=None, verbose_name=
        'Precio inicial de los derechos fiduciarios')
    rendimiento = models.FloatField(
        default=None, verbose_name='Rentabilidad estimada')
    foto = models.ImageField(upload_to='proyectos')
    descripcion = HTMLField()
    estado_comercial = models.CharField(
        max_length=1, choices=ESTADOS_COMERCIALES, default=1)
    estado = models.CharField(
        max_length=1, choices=ESTADOS, default=0)
    fiduciaria = models.ForeignKey(
        Fiduciaria, default=None)
    autor = models.ForeignKey(AUTH_USER_MODEL, default=None, null=True)
    slug = AutoSlugField(populate_from='nombre', null=True, unique=True)

    def __str__(self):
        return self.nombre
