from django.contrib import admin

from mercatitulo.proyectos.models import TipoProyecto
from mercatitulo.proyectos.models import MarcaProyecto
from mercatitulo.proyectos.models import Proyecto
from mercatitulo.derechos.utils import Send_mail

#admin.site.register(TipoProyecto)
@admin.register(TipoProyecto)
class AdminTipoProyecto(admin.ModelAdmin):
	#filtro, busqueda y listas
	search_fields = ('nombre',)
	list_display = ('id', 'nombre',)


#admin.site.register(MarcaProyecto)
@admin.register(MarcaProyecto)
class AdminMarcaProyecto(admin.ModelAdmin):
	#filtro, busqueda y listas
	search_fields = ('nombre',)
	list_display = ('id', 'nombre',)


#admin.site.register(Proyecto)
@admin.register(Proyecto)
class AdminProyecto(admin.ModelAdmin):
	search_fields = ('nombre',)
	list_filter = ('marca', 'estado_comercial', 'estado', 'fiduciaria',)
	list_display = ('id', 'nombre', 'estado', 'imagen', 'marca', 'estado_comercial', 
		'fiduciaria', 'precio', 'rendimiento', 'autor',)

	#mostrar imagen
	def imagen(self, obj):
		return '<img src="%s" width="150" height="100">' % obj.foto.url
	imagen.short_description = 'foto'
	imagen.allow_tags = True

	def save_model(self, request, obj, form, change):
		proyecto = Proyecto.objects.get(pk=obj.id)
		if form.cleaned_data.get('estado') == '1' and proyecto.estado == '0':
			Send_mail(
				{ 
					'proyecto_id': obj.id,
					'proyecto_name': obj.nombre,
					'username': obj.autor,
				},
				[obj.autor.email],
				'partials/email/aprobado_proyecto.txt',
				'partials/email/aprobado_proyecto.html',
				'Proyecto Aprobado',
			)
		obj.save()

