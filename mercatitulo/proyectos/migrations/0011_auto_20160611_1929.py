# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-12 00:29
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0010_auto_20160521_2111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyecto',
            name='estado_comercial',
            field=models.CharField(choices=[('0', 'Por construir'), ('1', 'En construcci\xf3n'), ('2', 'En operaci\xf3n')], default=1, max_length=1),
        ),
        migrations.AlterField(
            model_name='proyecto',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, null=True, populate_from='nombre', unique=True),
        ),
    ]
