from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from tinymce.models import HTMLField

@python_2_unicode_compatible
class Fiduciaria(models.Model):
    """ Clase para clasificar a los proyectos """
    nombre = models.CharField(default=None, max_length=50)
    descripcion = HTMLField()

    def __str__(self):
        return self.nombre
