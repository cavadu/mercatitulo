from django.contrib import admin

from mercatitulo.fiduciarias.models import Fiduciaria

admin.site.register(Fiduciaria)
