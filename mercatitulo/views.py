from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect

from mercatitulo.aliados.models import Aliado
from mercatitulo.proyectos.models import Proyecto


def Home(request):
    lista_de_aliados = Aliado.objects.all()
    lista_de_proyectos = Proyecto.objects.filter(estado=1)[:2]
    diccionario_template = {
        'proyectos': lista_de_proyectos,
        'aliados': lista_de_aliados,
    }
    return render(request, 'pages/home.html', {'content': diccionario_template})


def Landing(request):
    diccionario_template = {
        'empty': "",
    }
    return render(request, 'pages/landing.html', {'content': diccionario_template})


@login_required
def Logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return redirect('/')
