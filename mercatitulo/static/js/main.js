function classActive() {
	$(".nav-tabs li").bind('click', function() {
		if ($(this).hasClass('active')) {
			$(".nav-tabs li").removeClass('active');
			//$(this).addClass('active');
		} else {
			$(".nav-tabs li").removeClass('active');
			$(this).addClass('active');
		}
	});
}

$(document).ready(function(){
	$("#c_masP").bind('click', function() {
		if ($(".c_proyAnex").hasClass('active')) {
			$(".c_proyAnex").slideUp();
			$(".c_proyAnex").removeClass('active');
			$("#c_masP .glyphicon").removeClass('glyphicon-chevron-up');
			$("#c_masP .glyphicon").addClass('glyphicon-chevron-down');
		} else {
			$(".c_proyAnex").addClass('active');
			$(".c_proyAnex").slideDown();
			$("#c_masP .glyphicon").removeClass('glyphicon-chevron-down');
			$("#c_masP .glyphicon").addClass('glyphicon-chevron-up');
		}
	});

	classActive();
	var highestBox = 0;
	$(".d-proyecto-action-wrapper").each(function(index, el) {
		this_height = $("> div",this).height();
		if(this_height > highestBox){  
			highestBox = this_height;  
		}		
	});
	$(".d-proyecto-action-wrapper").each(function(index, el) {
		$("> div",this).height(highestBox);
	});
	vender_form();
	$(".field-precio").inputmask("currency", {
		digits: 0,
		radixPoint: ","
	});
	$(".field-rendimiento").inputmask("percentage", { radixPoint: "," });
});

function vender_form(){
	if ($("#id_negociable").val() == "0") {
		$(".field-minimo").hide();
	}
	$("#vender-form").each(function(index, el) {
		$("select", this).change(function() {
			changed_item = $(this);
			if (changed_item.is("#id_negociable")) {
				if ($("#id_negociable").val() == "0") {
					$("#id_precio_minimo").val('0');
					$(".field-minimo").hide();
				}
				else {
					$(".field-minimo").show();
				}
			}
		});
	});
	proyecto_field = $(".field-proyecto")
	disable_select(proyecto_field);
	$("#vender-form").each(function(index, el) {
		$("select", this).change(function() {
			changed_item = $(this);
			new_proyect_validation(this);
			if (changed_item.is('#id_tipo') || changed_item.is('#id_marca')) {
				if ($('#id_tipo').val() != "" && $('#id_marca').val() != "") {
					tipo = $('#id_tipo').val()
					marca = $('#id_marca').val()
					proyecto_field.load("/ajax/load_proyectos/?tipo=" + tipo + "&marca=" + marca + "&exclude=False", function(responseTxt, statusTxt, xhr){
						enable_select(proyecto_field);
					})
				}				
			}			
		});
	});
	proyecto_field_exclude = $(".field-proyecto-exclue")
	disable_select(proyecto_field_exclude);
	$("#vender-form").each(function(index, el) {
		$("select", this).change(function() {
			changed_item = $(this);
			new_proyect_validation(this);
			if (changed_item.is('#id_tipo') || changed_item.is('#id_marca')) {
				if ($('#id_tipo').val() != "" && $('#id_marca').val() != "") {
					tipo = $('#id_tipo').val()
					marca = $('#id_marca').val()
					proyecto_field_exclude.load("/ajax/load_proyectos/?tipo=" + tipo + "&marca=" + marca + "&exclude=True", function(responseTxt, statusTxt, xhr){
						enable_select(proyecto_field_exclude);
					})
				}				
			}			
		});
	});
}

function disable_select(selector){
	selector.each(function(index, el) {
		$("select", this).prop('disabled', 'disabled');
	});	
}

function enable_select(selector){
	selector.each(function(index, el) {
		$("select", this).prop('disabled', false);
	});		
}

$(document).on("change", "#id_proyecto", function(){
	new_proyect_validation(this);
});

function new_proyect_validation(item){
	changed_item = $(item);
	if(changed_item.is('#id_proyecto') && changed_item.val() == "new"){
		window.location.href = "/vender/proyecto_nuevo/"
	}
}

$(".df_mercado_desc_full span:nth-of-type(1)").text(function(index, currentText) {
    return currentText.substr(0, 200) + ' ...';
});

$(".df_mercado_desc_mid span:nth-of-type(1)").text(function(index, currentText) {
    return currentText.substr(0, 50) + ' ...';
});

$(".df_mercado_desc_last span:nth-of-type(1)").text(function(index, currentText) {
    return currentText.substr(0, 30) + ' ...';
});

$(".df_mercado_desc_full > a").click(function(e) {
	e.preventDefault();
	var data = $(this).attr("data");
	$("#modal_derechos_descripcion > p").text(data);
});

$(".df_mercado_desc_mid > a").click(function(e) {
	e.preventDefault();
	var data = $(this).attr("data");
	$("#modal_derechos_descripcion > p").text(data);
});

$(".df_mercado_desc_last > a").click(function(e) {
	e.preventDefault();
	var data = $(this).attr("data");
	$("#modal_derechos_descripcion > p").text(data);
});