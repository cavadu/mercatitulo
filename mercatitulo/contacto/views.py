from django.shortcuts import render
from django.shortcuts import redirect

from .forms import ContactoForm
from mercatitulo.users.models import User
from mercatitulo.derechos.utils import Send_mail


def Contacto(request):
	if request.method == "POST":
		form = ContactoForm(request.POST)
		if form.is_valid():
			nuevo_contacto = form.save()
			usuarios_correo = User.objects.filter(is_staff=True)
			destino_correo = []
			for usuario in usuarios_correo:
				destino_correo.append(str(usuario.email))
			Send_mail(
				{ 
					'contacto_name': nuevo_contacto.nombre,
					'contacto_email': nuevo_contacto.correo,
					'contacto_tel': nuevo_contacto.telefono,
				},
				destino_correo,
				'partials/email/nuevo_contacto_admin.txt',
				'partials/email/nuevo_contacto_admin.html',
				'Nuevo Contacto',
			)
			return render(request, 'pages/contacto_confirmar.html')
	else:
		form = ContactoForm()
	return render(request, 'forms/contacto.html', {'form': form})
