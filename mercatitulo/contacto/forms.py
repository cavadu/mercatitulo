# -*- coding: utf-8 -*-
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.bootstrap import FormActions

from .models import Contacto

class ContactoForm(forms.ModelForm):
	class Meta:
		model = Contacto
		fields = [
			'nombre',
			'telefono',
			'correo',
			'mensaje',
		]

	def __init__(self, *args, **kwargs):
		super(ContactoForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.layout.append(
			FormActions(Submit('save', 'Enviar'),)
		)