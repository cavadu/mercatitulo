from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from tinymce.models import HTMLField

@python_2_unicode_compatible
class Contacto(models.Model):
	nombre = models.CharField(max_length=255)
	telefono = models.CharField(max_length=255)
	correo = models.EmailField(max_length=255)
	mensaje = HTMLField()

	def __str__(self):
		return self.nombre
