from django.contrib import admin

from .models import Contacto

@admin.register(Contacto)
class AdminContacto(admin.ModelAdmin):
	search_fields = ('nombre', 'telefono',)
	list_display = ('id', 'nombre', 'telefono', 'correo', 'mensaje',)
