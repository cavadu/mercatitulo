from django.contrib import admin

from mercatitulo.derechos.models import Derecho, Ofertas, Compras
from mercatitulo.derechos.models import GaleriaMercado
from mercatitulo.derechos.views import RechazarOferta, AceptarOferta
from mercatitulo.derechos.utils import Send_mail

@admin.register(Derecho)
class AdminDerecho(admin.ModelAdmin):
	search_fields = ('nombre', 'autor',)
	list_filter = ('proyecto', 'frecuencia_pago', 'negociable', 'fecha_creacion', 'estado',)
	list_display = ('id', 'nombre', 'estado', 'proyecto', 'frecuencia_pago', 'autor', 'fecha_creacion',
					'precio', 'precio_minimo', 'monto_dividendo',)

	def save_model(self, request, obj, form, change):
		derecho = Derecho.objects.get(pk=obj.id)
		if form.cleaned_data.get('estado') == '1' and derecho.estado == '0':
			Send_mail(
				{ 
					'derecho_id': obj.id,
					'derecho_name': obj.nombre,
					'username': obj.autor,
				},
				[obj.autor.email],
				'partials/email/aprobado_derecho.txt',
				'partials/email/aprobado_derecho.html',
				'Derecho Aprobado',
			)
		obj.save()


@admin.register(Ofertas)
class AdminOfertas(admin.ModelAdmin):
	search_fields = ('autor', 'derecho',)
	list_filter = ('estado',)
	list_display = ('id', 'derecho', 'estado', 'valor', 'autor', 'correo', 'telefono')

	def save_model(self, request, obj, form, change):
		oferta = Ofertas.objects.get(pk=obj.id)
		if form.cleaned_data.get('estado') == '1' and oferta.estado == '0':
			obj.estado = '0'
			AceptarOferta(request, obj, obj.derecho)
			obj.estado = '1'
		elif form.cleaned_data.get('estado') == '2' and oferta.estado == '0':
			obj.estado = '0'
			RechazarOferta(request, obj, obj.derecho)
			obj.estado = '2'
		obj.save()

@admin.register(Compras)
class AdminCompras(admin.ModelAdmin):
	search_fields = ('id_derecho', 'derecho', 'id_proyecto', 'proyecto',
		'comprador', 'vendedor',)
	list_display = (
		'id',
		'id_derecho',
		'derecho',
		'valor_derecho',
		'id_proyecto',
		'proyecto',
		'valor_proyecto',
		'comprador',
		'correo_comprador',
		'telefono_comprador',
		'vendedor',
		'correo_vendedor',
		'telefono_vendedor')

admin.site.register(GaleriaMercado)
