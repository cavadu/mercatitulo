from django import template

from mercatitulo.derechos.models import GaleriaMercado


register = template.Library()

@register.inclusion_tag('partials/galeria_mercado.html')
def galeriaMercado():
	elementos_galeria = GaleriaMercado.objects.all()
	return {'galeria': elementos_galeria}
