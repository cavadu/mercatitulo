from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from autoslug import AutoSlugField
from tinymce.models import HTMLField


from mercatitulo.proyectos.models import Proyecto


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

NEGOCIABLE = (
    ('0', 'No'),
    ('1', 'Si'),
)

FRECUENCIA = (
    ('1', 'Anual'),
    ('12', 'Mensual'),
    ('6', 'Cada dos meses'),
    ('4', 'Cada tres meses'),
    ('2', 'Cada seis meses'),
)

ESTADOS = (
    ('0', 'Por aprobar'),
    ('1', 'Aprobado'),
    ('2', 'En proceso de compra'),
    ('3', 'Eliminado por el usuario'),
    ('4', 'Comprado'),
)

ESTADOS_OFERTAS = (
    ('0', 'En estudio'),
    ('1', 'Aceptada'),
    ('2', 'Rechazada'),
)

@python_2_unicode_compatible
class Derecho(models.Model):
    """Clase para gestionar los titulos fiduciarios"""
    nombre = models.CharField(
        max_length=140, verbose_name='Nombre del anuncio')
    proyecto = models.ForeignKey(
        Proyecto, default=None, verbose_name='Proyecto/Inmueble', limit_choices_to={'estado': 1})
    precio = models.FloatField(default=None, verbose_name='Precio de venta')
    precio_minimo = models.FloatField(
        default=None, verbose_name='Precio de venta minimo')
    monto_dividendo = models.FloatField(default=None)
    rentabilidad = models.FloatField(default=None, null=True)
    frecuencia_pago = models.CharField(
        max_length=2, choices=FRECUENCIA, default=1)
    ultimo_rendimiento = models.FloatField(default=None)
    negociable = models.CharField(
        max_length=1, choices=NEGOCIABLE, default=0)
    descripcion = HTMLField()
    autor = models.ForeignKey(AUTH_USER_MODEL, default=None, null=True)
    fecha_creacion = models.DateTimeField(auto_now=True)
    estado = models.CharField(
        max_length=1, choices=ESTADOS, default=0)
    slug = AutoSlugField(populate_from='nombre', null=True, editable=False, unique=True)

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class Compras(models.Model):
    id_derecho = models.CharField(max_length=255)
    derecho = models.ForeignKey(
        Derecho, default=None, verbose_name='Nombre del derecho')
    valor_derecho = models.FloatField(default=None, 
        verbose_name='Valor venta del derecho')
    id_proyecto = models.CharField(max_length=255)
    proyecto = models.CharField(max_length=255, verbose_name='Nombre del proyeto')
    valor_proyecto = models.FloatField(default=None)
    comprador = models.ForeignKey(AUTH_USER_MODEL, 
        related_name='usuario_comprador', default=None, null=True)
    correo_comprador = models.CharField(max_length=255, verbose_name='Correo del comprador')
    telefono_comprador = models.CharField(max_length=255, verbose_name='Telefono del comprador')
    vendedor = models.ForeignKey(AUTH_USER_MODEL, 
        related_name='usuario_vendedor', default=None, null=True)
    correo_vendedor = models.CharField(max_length=255, verbose_name='Correo del vendedor')
    telefono_vendedor = models.CharField(max_length=255, verbose_name='Telefono del vendedor')

    class Meta:
        verbose_name = 'Compras realizadas'
        verbose_name_plural = 'Compras realizadas'

    def __str__(self):
        return self.derecho.nombre

@python_2_unicode_compatible
class Ofertas(models.Model):
    autor = models.ForeignKey(AUTH_USER_MODEL, default=None, null=True)
    correo = models.CharField(max_length=255, verbose_name='Correo del usuario')
    telefono = models.CharField(max_length=255, verbose_name='telefono del usuario')
    derecho = models.ForeignKey(
        Derecho, default=None, limit_choices_to={'estado': 1})
    valor = models.FloatField(default=None, verbose_name='Valor de la oferta')
    estado = models.CharField(
        max_length=1, choices=ESTADOS_OFERTAS, default=0)
    llave = models.CharField(max_length=64, unique=True)

    class Meta:
        verbose_name = 'Ofertas por derecho'
        verbose_name_plural = 'Ofertas por derechos'

    def __str__(self):
        return self.derecho.nombre

@python_2_unicode_compatible
class GaleriaMercado(models.Model):
    """Clase para hacer administrables los testimonios"""
    titulo = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)
    foto = models.ImageField(upload_to='galeria')

    def __str__(self):
        return self.titulos
