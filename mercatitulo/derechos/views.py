# -*- coding: utf-8 -*-
from django.db.models import Q
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import Context
from django.utils.crypto import get_random_string
from django.core import urlresolvers

from mercatitulo.derechos.forms import BuscarDerechoForm, OfertarForm, DerechosForm
from mercatitulo.derechos.models import Derecho, Ofertas, Compras, FRECUENCIA, NEGOCIABLE, ESTADOS as estado_derecho, ESTADOS_OFERTAS
from mercatitulo.proyectos.models import TipoProyecto, ESTADOS_COMERCIALES, ESTADOS as estado_proyecto
from mercatitulo.users.models import User
from mercatitulo.derechos.utils import Cast, Send_mail

def RechazarOferta(request, oferta, derecho):
    if oferta.estado == '0':
        Ofertas.objects.filter(pk=oferta.id).update(estado='2')
        Send_mail(
            { 
                'derecho_id': derecho.id,
                'derecho_name': derecho.nombre,
                'oferta_valor': oferta.valor,
            },
            [oferta.correo],
            'partials/email/oferta_rechazo.txt',
            'partials/email/oferta_rechazo.html',
            'Oferta Rechazada'
        )
    else:
        messages.warning(request, 'El estado de la oferta ya ha sido modificado')

def EliminarDerechos(request, slug):
    derecho = get_object_or_404(Derecho, slug=slug)
    data = {
        'derecho': derecho,
    }
    if request.method == "POST":
        derecho.estado = '3'
        derecho.save()
        ofertas = Ofertas.objects.filter(derecho=derecho.id, estado='0')
        for oferta in ofertas:
            RechazarOferta(request, oferta, derecho)
        messages.success(request, 'El derecho ha sido eliminado')
        redirect_to = '/mi-cuenta/?opc=derechos'
        return redirect(redirect_to)
    else:
        return render(request, 'pages/eliminar_derechos.html', {'data': data})
    

def EditarDerechos(request, slug):
    derecho = get_object_or_404(Derecho, slug=slug)
    data = {
        'derecho': derecho,
    }
    if request.method == "POST":
        post = request.POST.copy()
        if not post['precio'] == '$ 0':
            post['precio'] = Cast(post['precio'])
        if not post['precio_minimo'] == '$ 0':
            post['precio_minimo'] = Cast(post['precio_minimo'])
        else:
            if post['negociable'] == '0':
                post['precio_minimo'] = '0'   
        if not post['monto_dividendo'] == '$ 0':
            post['monto_dividendo'] = Cast(post['monto_dividendo'])
        if not post['ultimo_rendimiento'] == '$ 0':
            post['ultimo_rendimiento'] = Cast(post['ultimo_rendimiento'])
        form = DerechosForm(post, instance=derecho)
        if form.is_valid():
            form.save()
            derecho.estado = 0
            derecho.rentabilidad = (derecho.ultimo_rendimiento * 
                int(derecho.frecuencia_pago) / derecho.precio)
            derecho.save()
            messages.success(request, 'El derecho ha sido modificado - debe ser aprobado nuevamente')
            redirect_to = '/mi-cuenta/?opc=derechos'
            return redirect(redirect_to)
    else:
        form = DerechosForm(instance=derecho)
    return render(request, 'forms/editar_derecho.html', {'form': form, 'data': data})

def Vender(request):
    if request.method == "POST":
        post = request.POST.copy()
        if not post['precio'] == '$ 0':
            post['precio'] = Cast(post['precio'])
        if not post['precio_minimo'] == '$ 0':
            post['precio_minimo'] = Cast(post['precio_minimo'])
        else:
            if post['negociable'] == '0':
                post['precio_minimo'] = '0'   
        if not post['monto_dividendo'] == '$ 0':
            post['monto_dividendo'] = Cast(post['monto_dividendo'])
        if not post['ultimo_rendimiento'] == '$ 0':
            post['ultimo_rendimiento'] = Cast(post['ultimo_rendimiento'])
        form = DerechosForm(post)
        if form.is_valid():
            nuevo_derecho = form.save()
            nuevo_derecho.rentabilidad = (nuevo_derecho.ultimo_rendimiento * 
                int(nuevo_derecho.frecuencia_pago) / nuevo_derecho.precio)
            nuevo_derecho.save()
            request.session['derecho_id'] = nuevo_derecho.id
            if request.user.is_authenticated():
                derecho = Derecho.objects.get(pk=nuevo_derecho.id)
                derecho.autor_id = request.user.id
                derecho.save()
                Send_mail(
                    { 
                        'derecho_id': nuevo_derecho.id,
                        'derecho_name': nuevo_derecho.nombre,
                        'username': request.user.username,
                    },
                    [request.user.email],
                    'partials/email/asignacion_derecho.txt',
                    'partials/email/asignacion_derecho.html',
                    'Derecho asignado',
                )
                redirect_to = '/vender/confirmacion/'
            else:
                redirect_to = '/accounts/signup/'
            usuarios_correo = User.objects.filter(is_staff=True)
            destino_correo = []
            for usuario in usuarios_correo:
                destino_correo.append(str(usuario.email))
            if request.user.username:
                usuario = request.user.username
            else:
                usuario = 'Pendiente asignar usuario'
            url_aprobar = request.build_absolute_uri(
                urlresolvers.reverse("admin:derechos_derecho_change",
                args=(nuevo_derecho.id,)))
            Send_mail(
                { 
                    'derecho_id': nuevo_derecho.id,
                    'derecho_name': nuevo_derecho.nombre,
                    'username': usuario,
                    'url_aprobar': url_aprobar,
                },
                destino_correo,
                'partials/email/asignacion_derecho_admin.txt',
                'partials/email/asignacion_derecho_admin.html',
                'Derecho asignado',
            )
            return redirect(redirect_to)
    else:
        form = DerechosForm()
    return render(request, 'forms/vender.html', {'form': form})

def Confirmacion(request):
    if request.session.get('derecho_id', None):
        derecho = Derecho.objects.get(pk=request.session['derecho_id'])
        del request.session['derecho_id']
        data = {
            'derecho': derecho,
            'estado': dict(estado_derecho).get(derecho.estado),
        }
        return render(request, 'pages/confirmacion.html', {'data': data})
    else:
        redirect_to = '/'
        return redirect(redirect_to)

def Mercado(request):
    formbuscar = BuscarDerechoForm()
    proyecto_id = ""
    codigo_id = ""
    tipos_proyecto_id = ""
    if 'proyecto' in request.GET.keys() and 'codigo' in request.GET.keys() and 'tipos_proyecto' in request.GET.keys():
        proyecto_id = request.GET['proyecto']
        codigo_id = request.GET['codigo']
        tipos_proyecto_id = request.GET['tipos_proyecto']
        formbuscar = BuscarDerechoForm(initial=request.GET)
        if proyecto_id and codigo_id and tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(pk=codigo_id) | 
                Q(proyecto=proyecto_id) | 
                Q(proyecto__tipo__id=tipos_proyecto_id),
                autor__isnull=False,
                estado=1)
        elif proyecto_id and codigo_id and not tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(pk=codigo_id) | 
                Q(proyecto=proyecto_id),
                autor__isnull=False,
                estado=1)
        elif proyecto_id and not codigo_id and tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(proyecto=proyecto_id) | 
                Q(proyecto__tipo__id=tipos_proyecto_id),
                autor__isnull=False,
                estado=1)
        elif proyecto_id and not codigo_id and not tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(proyecto=proyecto_id),
                autor__isnull=False,
                estado=1)
        elif not proyecto_id and codigo_id and tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(pk=codigo_id) | 
                Q(proyecto__tipo__id=tipos_proyecto_id),
                autor__isnull=False,
                estado=1)
        elif not proyecto_id and codigo_id and not tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(pk=codigo_id),
                autor__isnull=False,
                estado=1)
        elif not proyecto_id and not codigo_id and tipos_proyecto_id:
            derechos = Derecho.objects.filter(
                Q(proyecto__tipo__id=tipos_proyecto_id),
                autor__isnull=False,
                estado=1)
        else:
            derechos = Derecho.objects.filter(estado=1, autor__isnull=False)
    else:
        derechos = Derecho.objects.filter(estado=1, autor__isnull=False)

    tipos_de_proyecto = TipoProyecto.objects.all().order_by('nombre')
    data = {
        'derechos': derechos,
        'tipos_proyecto': tipos_de_proyecto,
        'buscar_derecho_form': formbuscar,
        'proyecto_id': proyecto_id,
        'codigo_id': codigo_id,
        'tipos_proyecto_id': tipos_proyecto_id,
    }
    return render(request, 'pages/mercado.html', {'data': data})

def correoCompra(request, derecho, user):
    usuarios_correo = User.objects.filter(is_staff=True)
    destino_correo = []
    for usuario in usuarios_correo:
        destino_correo.append(str(usuario.email))
    Send_mail(
        { 
            'derecho_id': derecho.id,
            'derecho_nombre': derecho.nombre,
            'derecho_precio': derecho.precio,
            'derecho_precio_minimo': derecho.precio_minimo,
            'derecho_monto_dividendo': derecho.monto_dividendo,
            'derecho_frecuencia_pago': dict(FRECUENCIA).get(derecho.frecuencia_pago),
            'derecho_ultimo_rendimiento': derecho.ultimo_rendimiento,
            'derecho_negociable': dict(NEGOCIABLE).get(derecho.negociable),
            'derecho_autor': derecho.autor,
            'derecho_fecha_creacion': derecho.fecha_creacion,
            'derecho_estado': dict(estado_derecho).get('%s' % derecho.estado),

            'derecho_proyecto_nombre': derecho.proyecto.nombre,
            'tipos':  derecho.proyecto.tipo.all(),
            'derecho_proyecto_marca': derecho.proyecto.marca,
            'derecho_proyecto_precio': derecho.proyecto.precio,
            'derecho_proyecto_rendimiento': derecho.proyecto.rendimiento,
            'derecho_proyecto_estado_comercial': dict(ESTADOS_COMERCIALES).get(derecho.proyecto.estado_comercial),
            'derecho_proyecto_estado': dict(estado_proyecto).get(derecho.proyecto.estado),
            'derecho_proyecto_fiduciaria': derecho.proyecto.fiduciaria,

            'comprador_username': user.username,
            'comprador_first_name': user.first_name,
            'comprador_last_name': user.last_name,
            'comprador_telefono': user.telefono,
            'comprador_ciudad': user.ciudad,
            'comprador_email': user.email,

            'vendedor_username': derecho.autor.username,
            'vendedor_first_name': derecho.autor.first_name,
            'vendedor_last_name': derecho.autor.last_name,
            'vendedor_telefono': derecho.autor.telefono,
            'vendedor_ciudad': derecho.autor.ciudad,
            'vendedor_email': derecho.autor.email,
        },
        destino_correo,
        'partials/email/compra_derecho.txt',
        'partials/email/compra_derecho.html',
        'Compra de derecho'
    )

def registrarCompras(request, derecho, comprador, valor_derecho):
    Compras.objects.create(
        id_derecho=derecho.id,
        derecho=derecho,
        valor_derecho=valor_derecho,
        id_proyecto=derecho.proyecto.id,
        proyecto=derecho.proyecto,
        valor_proyecto=derecho.proyecto.precio,
        comprador=comprador,
        correo_comprador=comprador.email,
        telefono_comprador=comprador.telefono,
        vendedor=derecho.autor,
        correo_vendedor=derecho.autor.email,
        telefono_vendedor=derecho.autor.telefono)

@login_required
def detalleCompra(request):
    derecho_id = request.COOKIES.get('derecho_id')
    derecho = Derecho.objects.get(pk=derecho_id)
    data = {
            'derecho': derecho
        }
    if request.method == "POST":
        response = render(request, 'pages/compra_confirmada.html', {'data': data})
        response.delete_cookie('derecho_id')
        derecho.estado = 2
        derecho.save()
        registrarCompras(request, derecho, request.user, derecho.precio)
        ofertas = Ofertas.objects.filter(derecho=derecho.id, estado='0')
        for oferta in ofertas:
            RechazarOferta(request, oferta, derecho)
        correoCompra(request, derecho, request.user)
        return response
    else:
        return render(request, 'pages/compra.html', {'data': data})

def Comprar(request):
    if request.method == "POST":
        if request.user.is_authenticated():
            redirect_to = '/detalle-compra/'
        else:
            redirect_to = '/accounts/login/'
        response = redirect(redirect_to)
        response.set_cookie('derecho_id', request.POST.get('derecho_id'))
        return response
    else:
        redirect_to = '/mercado-derechos/'
    return redirect(redirect_to)

def AceptarOferta(request, oferta, derecho):
    if oferta.estado == '0':
        Ofertas.objects.filter(pk=oferta.id).update(estado='1')
        Derecho.objects.filter(id=derecho.id).update(estado='2')
        registrarCompras(request, derecho, oferta.autor, oferta.valor)
        correoCompra(request, derecho, oferta.autor)
        Send_mail(
            { 
                'derecho_id': derecho.id,
                'derecho_name': derecho.nombre,
                'oferta_valor': oferta.valor,
            },
            [oferta.correo],
            'partials/email/oferta_aceptar.txt',
            'partials/email/oferta_aceptar.html',
            'Oferta Aceptada'
        )
        Ofertas.objects.filter(derecho=derecho.id).exclude(pk=oferta.id).update(estado='2')
        ofertas_rechazadas = Ofertas.objects.filter(derecho=derecho.id, estado='2')
        for ofertas in ofertas_rechazadas:
            Send_mail(
                { 
                    'derecho_id': derecho.id,
                    'derecho_name': derecho.nombre,
                    'oferta_valor': ofertas.valor,
                },
                [ofertas.correo],
                'partials/email/oferta_rechazo.txt',
                'partials/email/oferta_rechazo.html',
                'Oferta Rechazada'
            )
    else:
        messages.warning(request, 'El estado de la oferta ya ha sido modificado')

@login_required
def ConfirmOferta(request, llave):
    if request.user.is_staff:
        oferta = Ofertas.objects.get(llave=llave)
        derecho = Derecho.objects.get(pk=oferta.derecho.id)
        data = {
            'oferta': oferta,
            'derecho': derecho,
            'estado_oferta': dict(ESTADOS_OFERTAS).get(oferta.estado),
        }
        if request.method == "POST":
            if 'aceptar' in request.POST:
                AceptarOferta(request, oferta, derecho)
                pagina = 'pages/oferta_aceptada.html'
            elif 'rechazar' in request.POST:
                RechazarOferta(request, oferta, derecho)
                pagina = 'pages/oferta_rechazada.html'
            return render(request, pagina, {'data': data})
        else:
            return render(request, 'pages/confirm_oferta.html', {'data': data})
    else:
        redirect_to = '/'
        return redirect(redirect_to)

@login_required
def SaveOferta(request):
    derecho_id = request.COOKIES.get('derecho_id')
    derecho = Derecho.objects.get(pk=derecho_id)
    valor = request.COOKIES.get('valor')
    data = {
            'derecho': derecho
        }
    response = render(request, 'pages/oferta_confirmada.html', {'data': data})
    response.delete_cookie('derecho_id')
    response.delete_cookie('valor')
    llave = get_random_string(64).lower()
    oferta = Ofertas.objects.create(
        autor=request.user,
        correo=request.user.email,
        telefono=request.user.telefono,
        derecho=derecho,
        valor=valor,
        llave=llave)
    usuarios_correo = User.objects.filter(is_staff=True)
    destino_correo = []
    for usuario in usuarios_correo:
        destino_correo.append(str(usuario.email))
    url_llave = request.build_absolute_uri(llave)
    Send_mail(
        { 
            'derecho_id': derecho.id,
            'derecho_name': derecho.nombre,
            'username': request.user.username,
            'url_llave': url_llave,
        },
        destino_correo,
        'partials/email/oferta_derecho.txt',
        'partials/email/oferta_derecho.html',
        'Oferta de derecho'
    )
    return response

def Ofertar(request, slug):
    derecho = get_object_or_404(Derecho, slug=slug)
    data = {
        'derechos': derecho,
    }
    if request.method == "POST":
        post = request.POST.copy()
        if not post['valor'] == '$ 0':
            post['valor'] = Cast(post['valor'])
            valor = post['valor']
            if post['valor'] < derecho.precio_minimo:
                post['valor'] = '$ 0'
                messages.warning(request, 'El valor ingresado es inferior al valor mínimo')
        form = OfertarForm(post)
        if form.is_valid():
            if request.user.is_authenticated():
                redirect_to = '/save-oferta/'
            else:
                redirect_to = '/accounts/login/'
            response = redirect(redirect_to)
            response.set_cookie('derecho_id', derecho.pk)
            response.set_cookie('valor', valor)
            return response
    else:
        form = OfertarForm()
    return render(request, 'pages/detalle_oferta.html', {'form': form, 'data': data})

def Detalle(request, slug):
    derecho = get_object_or_404(Derecho, slug=slug)
    data = {
        'derechos': derecho,
    }
    return render(request, 'pages/detalle.html', {'data': data})
