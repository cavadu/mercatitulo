from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

def Cast(value):
	castedvalue = value.replace(
		'.', '').replace(
		'$','').replace(
		' ','').replace(
		'%', '').replace(
		',', '.')
	castedvalue = float(castedvalue)
	return castedvalue
	
def Send_mail(context, to, text_tamplate, html_template, subject):
	texto_plano = get_template(text_tamplate)
	texto_html = get_template(html_template)
	variables = (context)
	subject = subject
	from_email = settings.DEFAULT_FROM_EMAIL
	text_content = texto_plano.render(variables)
	html_content = texto_html.render(variables)
	msg = EmailMultiAlternatives(subject, text_content, from_email, to)
	msg.attach_alternative(html_content, "text/html")
	msg.send()
        