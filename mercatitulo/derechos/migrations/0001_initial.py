# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-19 00:26
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('proyectos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Derecho',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('costo', models.FloatField(default=None)),
                ('costo_minimo', models.FloatField(default=None)),
                ('rendimiento', models.FloatField(default=None)),
                ('cierre_anterior', models.FloatField(default=None)),
                ('negociable', models.CharField(choices=[('0', 'No'), ('1', 'Si')], default=1, max_length=1)),
                ('descripcion', tinymce.models.HTMLField()),
                ('fecha_creacion', models.DateTimeField(auto_now=True)),
                ('estado', models.CharField(choices=[('0', 'Por aprobar'), ('1', 'Aprobado')], default=0, max_length=1)),
                ('autor', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('proyecto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='proyectos.Proyecto')),
            ],
        ),
        migrations.CreateModel(
            name='GaleriaMercado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=255)),
                ('descripcion', models.CharField(max_length=255)),
                ('foto', models.ImageField(upload_to='galeria')),
            ],
        ),
    ]
