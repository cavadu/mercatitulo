# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-07 13:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('derechos', '0012_auto_20160707_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ofertas',
            name='llave',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
