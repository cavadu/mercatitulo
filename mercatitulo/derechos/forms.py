# -*- coding: utf-8 -*-
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from mercatitulo.derechos.models import Derecho, Ofertas
from mercatitulo.proyectos.models import MarcaProyecto
from mercatitulo.proyectos.models import Proyecto
from mercatitulo.proyectos.models import TipoProyecto


class DerechosForm(forms.ModelForm):
    marca = forms.ModelChoiceField(
        queryset=MarcaProyecto.objects.all(), label="Marca comercial")
    tipo = forms.ModelChoiceField(
        queryset=TipoProyecto.objects.all(), label="Tipo de inmueble")

    class Meta:
        model = Derecho
        widgets = {
            'precio': forms.TextInput(attrs={'class': 'field-precio'}),
            'monto_dividendo': forms.TextInput(attrs={'class': 'field-precio'}),
            'ultimo_rendimiento': forms.TextInput(attrs={'class': 'field-precio'}),
            'precio_minimo': forms.TextInput(attrs={'class': 'field-precio'}),
        }
        exclude = [
            'autor',
            'fecha_creacion',
            'estado',
            'rentabilidad',
            'slug',
        ]

class OfertarForm(forms.ModelForm):
    class Meta:
        model = Ofertas
        widgets = {
            'valor': forms.TextInput(attrs={'class': 'field-precio'}),
        }
        fields = [
            'valor',
        ]

class BuscarDerechoForm(forms.Form):
    proyecto = forms.ModelChoiceField(queryset=Proyecto.objects.filter(estado=1))
    codigo = forms.IntegerField()
    tipos_proyecto = forms.IntegerField(
        widget=forms.HiddenInput(), required=False)
    helper = FormHelper()
    helper.label_class = 'hide'
    helper.form_method = 'GET'
    helper.add_input(Submit('enviar', 'Buscar', css_class='btn-primary'))

    def __init__(self, *args, **kwargs):
        super(BuscarDerechoForm, self).__init__(*args, **kwargs)
        self.fields['codigo'].widget.attrs[
            'placeholder'] = "Código derecho fiduciario"
        self.fields['proyecto'].empty_label = "Seleccione un proyecto"
