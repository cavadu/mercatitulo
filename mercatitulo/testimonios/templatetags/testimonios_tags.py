from django import template

from mercatitulo.testimonios.models import Testimonio


register = template.Library()

@register.inclusion_tag('partials/testimonios.html')
def lista_testimonios():
	lista_de_testimonios = Testimonio.objects.all()
	return {'testimonios': lista_de_testimonios}
