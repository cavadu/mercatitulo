from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Testimonio(models.Model):
	"""Clase para hacer administrables los testimonios"""
	nombre = models.CharField(max_length=255)
	cargo = models.CharField(max_length=255)
	foto = models.ImageField(upload_to='testimonios')
	descripcion = models.TextField(blank=True)
	
	def __str__(self):
  		return self.nombre  		