from django.contrib import admin

from mercatitulo.aliados.models import Aliado

#admin.site.register(Aliado)
@admin.register(Aliado)
class AdminAliado(admin.ModelAdmin):
	#filtros, busqueda y campos
	list_display = ('id', 'nombre', 'imagen', 'posicion',)
	search_fields = ('nombre',)
	list_filter = ('posicion',)

	#mostrar imagen
	def imagen(self, obj):
		return '<img src="%s" width="100" height="100">' % obj.foto.url
	imagen.short_description = 'foto'
	imagen.allow_tags = True
