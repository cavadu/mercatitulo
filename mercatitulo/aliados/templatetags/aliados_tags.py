from django import template

from mercatitulo.aliados.models import Aliado


register = template.Library()

@register.inclusion_tag('partials/aliados.html')
def lista_aliados():
	lista_de_aliados = Aliado.objects.all()
	return {'aliados': lista_de_aliados}
