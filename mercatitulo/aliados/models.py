from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Aliado(models.Model):
    """Clase para hacer administrables los proyectos"""
    nombre = models.CharField(max_length=255)
    foto = models.ImageField(upload_to='aliado')
    posicion = models.FloatField(default=0)

    def __str__(self):
        return self.nombre
