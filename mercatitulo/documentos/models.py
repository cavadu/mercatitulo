from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from tinymce.models import HTMLField

@python_2_unicode_compatible
class Descargadocs(models.Model):
	nombre = models.CharField(max_length=255)
	descripcion = HTMLField()
	archivo = models.FileField(upload_to='docs_derechos_compra/descargas')

	class Meta:
		verbose_name = 'Admin documentos a descargar - Compra de Derechos'
		verbose_name_plural = 'Admin documentos a descargar - Compra de Derechos'

	def __str__(self):
		return self.nombre
