from django.contrib import admin

from .models import Descargadocs

@admin.register(Descargadocs)
class AdminDescargadocs(admin.ModelAdmin):
	search_fields = ('nombre',)
	list_display = ('id', 'archivos', 'descripcion',)

	def archivos(self, obj):
		return '<a href="%s" target="_blank">%s</a>' % (obj.archivo.url, obj.nombre)
	archivos.short_description = 'archivos'
	archivos.allow_tags = True