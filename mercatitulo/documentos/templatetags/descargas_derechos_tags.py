from django import template

from mercatitulo.documentos.models import Descargadocs

register = template.Library()

@register.inclusion_tag('partials/docs_descarga_derechos.html')
def lista_descarga_archvios_derechos():
	archivos_descargar = Descargadocs.objects.all()
	return {'descargadocs_derechos': archivos_descargar}