from django import template

from mercatitulo.users.models import User


register = template.Library()

@register.simple_tag
def usuariosDetalle(userid):
	detalle_usuario = User.objects.get(username=userid)
	return detalle_usuario
