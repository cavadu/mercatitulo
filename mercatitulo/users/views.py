# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import redirect
from django.shortcuts import render

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from allauth.account.views import SignupView
from allauth.account.views import LoginView
from allauth.account.utils import complete_signup
from allauth.account import app_settings
from allauth.exceptions import ImmediateHttpResponse
from mercatitulo.derechos.models import Derecho, Ofertas, Compras
from mercatitulo.proyectos.models import Proyecto

from .models import User
from .forms import LoginFormMercatitulo, MicuentaForm
from mercatitulo.derechos.utils import Send_mail


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'
    

class SignupMercatitulo(SignupView):
    def asignar_usuario(self, objeto, tag_id, tag_name, tag_txt, tag_html, tag_subject):
        if objeto.autor_id is None:
            objeto.autor_id = self.user
            objeto.save()
            Send_mail(
                { 
                    tag_id: objeto.id,
                    tag_name: objeto.nombre,
                    'username': self.user.username,
                },
                [self.user.email],
                tag_txt,
                tag_html,
                tag_subject,
            )

    def form_valid(self, form):
        self.user = form.save(self.request)
        if self.request.session.get('derecho_id', None):
            self.asignar_usuario(
                Derecho.objects.get(pk=self.request.session['derecho_id']),
                'derecho_id',
                'derecho_name',
                'partials/email/asignacion_derecho.txt',
                'partials/email/asignacion_derecho.html',
                'Derecho asignado',
                )
        if self.request.session.get('proyecto_id', None):
            self.asignar_usuario(
                Proyecto.objects.get(pk=self.request.session['proyecto_id']),
                'proyecto_id',
                'proyecto_name',
                'partials/email/asignacion_proyecto.txt',
                'partials/email/asignacion_proyecto.html',
                'Nuevo Proyecto',
                )
        return complete_signup(self.request, self.user,
                                app_settings.EMAIL_VERIFICATION,
                                self.get_success_url())


class LoginMercatitulo(LoginView):
    form_class = LoginFormMercatitulo

@login_required
def MiCuenta(request):
    usuario = User.objects.get(pk=request.user.id)
    proyectos = Proyecto.objects.filter(autor=request.user.id)
    derechos = Derecho.objects.filter(autor=request.user.id).exclude(
                estado=3)
    ofertas = Ofertas.objects.filter(autor=request.user.id)
    compras = Compras.objects.filter(comprador=request.user.id)
    if request.method == "POST":
        form = MicuentaForm(request.POST)
        opcion = 'cuenta'
        if form.is_valid():
            usuario = User.objects.get(pk=request.user.id)
            usuario.first_name = form.cleaned_data['nombre']
            usuario.last_name = form.cleaned_data['apellido']
            usuario.telefono = form.cleaned_data['telefono']
            usuario.ciudad = form.cleaned_data['ciudad']
            usuario.email = form.cleaned_data['correo']
            usuario.save()
            messages.success(request, 'Tus datos han sido actualizados')
    else:
        form = MicuentaForm(initial={
            'nombre': request.user.first_name,
            'apellido': request.user.last_name,
            'telefono': request.user.telefono,
            'ciudad': request.user.ciudad,
            'correo': request.user.email})
        if 'opc' in request.GET.keys():
            opcion = request.GET['opc']
        else:
            opcion = 'cuenta'
    data = {
        'usuario': usuario,
        'proyectos': proyectos,
        'derechos': derechos,
        'ofertas': ofertas,
        'compras': compras,
        'opcion': opcion,
        'form': form
    }
    return render(request, 'pages/micuenta.html', {'data': data})