# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import redirect
from nocaptcha_recaptcha.fields import NoReCaptchaField
from django.template.loader import get_template
from django.template import Context

from allauth.account.forms import LoginForm
from allauth.account.utils import perform_login
from allauth.account import app_settings

from mercatitulo.derechos.models import Derecho
from mercatitulo.proyectos.models import Proyecto
from mercatitulo.derechos.utils import Send_mail
from .models import User

class MicuentaForm(forms.Form):
	widget_nombre = forms.TextInput(attrs={'placeholder': 'Modifica nombre', 
											'autofocus': 'autofocus'})
	widget_apellido = forms.TextInput(attrs={'placeholder': 'Modifica apellido'})
	widget_telefono = forms.TextInput(attrs={'placeholder': 'Modifica teléfono'})
	widget_ciudad = forms.TextInput(attrs={'placeholder': 'Modifica ciudad'})
	widget_correo = forms.EmailInput(attrs={'placeholder': 'Modifica email'})

	nombre = forms.CharField(widget=widget_nombre, max_length=255)
	apellido = forms.CharField(widget=widget_apellido, max_length=255)
	telefono = forms.CharField(widget=widget_telefono, max_length=255)
	ciudad = forms.CharField(widget=widget_ciudad, max_length=255)
	correo = forms.EmailField(widget=widget_correo)

class SignupForm(forms.Form):

	widget_nombre = forms.TextInput(attrs={'placeholder': 'Su nombre', 
											'autofocus': 'autofocus'})
	widget_apellido = forms.TextInput(attrs={'placeholder': 'Su apellido'})
	widget_telefono = forms.TextInput(attrs={'placeholder': 'Su teléfono'})
	widget_ciudad = forms.TextInput(attrs={'placeholder': 'Ciudad de residencia'})

	nombre = forms.CharField(widget=widget_nombre, max_length=255)
	apellido = forms.CharField(widget=widget_apellido, max_length=255)
	telefono = forms.CharField(widget=widget_telefono, max_length=255)
	ciudad = forms.CharField(widget=widget_ciudad, max_length=255)
	captcha = NoReCaptchaField(label='')

	def signup(self, request, user):
		user.first_name = self.cleaned_data['nombre']
		user.last_name = self.cleaned_data['apellido']
		user.telefono = self.cleaned_data['telefono']
		user.ciudad = self.cleaned_data['ciudad']
		user.save()

class LoginFormMercatitulo(LoginForm):
	def asignar_usuario(self, objeto, tag_id, tag_name, tag_txt, tag_html, tag_subject):
		if objeto.autor_id is None:
			objeto.autor_id = self.user.id
			objeto.save()
			Send_mail(
				{ 
					tag_id: objeto.id,
					tag_name: objeto.nombre,
					'username': self.user.username,
				},
				[self.user.email],
				tag_txt,
				tag_html,
				tag_subject,
			)

	def login(self, request, redirect_url=None):
		redirect_url = '/'
		ret = perform_login(request, self.user,
							email_verification=app_settings.EMAIL_VERIFICATION,
							redirect_url=redirect_url)

		if request.user.is_authenticated():
			if self.request.session.get('derecho_id', None):
				self.asignar_usuario(
					Derecho.objects.get(pk=self.request.session['derecho_id']),
					'derecho_id',
					'derecho_name',
					'partials/email/asignacion_derecho.txt',
					'partials/email/asignacion_derecho.html',
					'Derecho asignado',
					)
				redirect_to = '/vender/confirmacion/'
				return redirect(redirect_to)
			elif self.request.session.get('proyecto_id', None):
				self.asignar_usuario(
					Proyecto.objects.get(pk=self.request.session['proyecto_id']),
					'proyecto_id',
					'proyecto_name',
					'partials/email/asignacion_proyecto.txt',
					'partials/email/asignacion_proyecto.html',
					'Nuevo Proyecto',
					)
				redirect_to = '/proyecto/confirmacion/'
				return redirect(redirect_to)
			elif 'valor' in self.request.COOKIES and 'derecho_id' in self.request.COOKIES:
				redirect_to = '/save-oferta/'
				return redirect(redirect_to)
			elif 'derecho_id' in self.request.COOKIES:
				redirect_to = '/detalle-compra/'
				return redirect(redirect_to)
		remember = app_settings.SESSION_REMEMBER
		if remember is None:
			remember = self.cleaned_data['remember']
		if remember:
			request.session.set_expiry(app_settings.SESSION_COOKIE_AGE)
		else:
			request.session.set_expiry(0)
		return ret